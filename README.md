# App

Application consists of three elements

- server, run by `python manage.py runserver`, serving json api
- worker, run by `python manage.py rundramatiq`, starting background workers,
- scheduler, run by `python manage.py runschedule`, used to trigger periodic tasks on worker.

As this is a preview app I did not try to implement full error handling or similar stuff.

I did not write any tests, as I am not familiar enough with Django yet to do so efficiently.

I did no try to change default database from SQLite, however the app requires RabbitMQ running.

## API

There are two api endpoints, as required by requirements

- Companies, showing company data and last recorded statistic, behind `/companies/`
- Statistics, showing all statistics for all companies for future use, behind `/statistics/`.

There should be also third one, `/companies/<id>/statistics/` showing all statististics for a company, but I had a little problem with setting hierchical API point in Django Rest Framework, and it would be few more hours of work to understand exactly how it works.

There was also optional story to make with real time data updates.
It should be done with entrypoint directly requesting to external API, but I did not do it.
Or some pubsub pattern could be used, this should lessen server bandwidth when neccesary.

## Mistakes / things to do
SQLite does have limited connection pool and it does happen to be exceeded by workers, so sometimes they raise excaption due to that. It's not a problem, because adter task retry it will most likely work.

Hardcoded variables should be moved to environmental variables and Docker config should be created with docker-compose to allow one command deployment with unified URL behind reverse proxy.

Client should refresh company list periodically.

Error handling and validation is basically nonexistant. For instance, API should not allow symbol change after creation.

Moving fetching initial data to async worker seems like mistake, it does not allow error handling when company not found.

I could not find information about trading hours, so I decided to ignore it completely and treat all data as final. Also updating all 100 days each seems way too unneccesary.
