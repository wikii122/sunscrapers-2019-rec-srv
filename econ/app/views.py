from rest_framework import viewsets
from econ.app.models import Company, Statistics
from econ.app.serializers import CompanySerializer, CompanyStatisticsSerializer
from econ.app.tasks import fetch_company_data


class CompanyInformationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows companies to be viewed or edited.
    """

    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    class Meta:
        ordering = ["-id"]

    def create(self, request, *args, **kwargs):
        result = super(CompanyInformationViewSet, self).create(request, *args, **kwargs)
        fetch_company_data.send(result.data["symbol"])
        return result


class CompanyStatisticsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows to view company statistics.
    """

    queryset = Statistics.objects.all()
    serializer_class = CompanyStatisticsSerializer
