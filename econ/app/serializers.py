from rest_framework import serializers

from econ.app.models import Company, Statistics


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    last_statistic = serializers.SerializerMethodField()

    def get_last_statistic(self, company):
        statistic = company.statistics_set.order_by('-date').first()
        serializer = CompanyStatisticsSerializer(statistic, context=self.context)
        return serializer.data

    class Meta:
        model = Company
        fields = ("url", "name", "symbol", "website", "logo", 'last_statistic')
        read_only_fields = ("name", "website", "logo", 'last_statistic')


class CompanyStatisticsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Statistics
        fields = ("company", "date", "high", "low", "open", "close", "volume")
        read_only_fields = ("company", "date", "high", "low", "open", "close", "volume")
