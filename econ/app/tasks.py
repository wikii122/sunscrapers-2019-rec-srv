import datetime
import dramatiq
import logging
import requests

from django.conf import settings
from django.core import exceptions

from econ.app.models import Company, Statistics


@dramatiq.actor
def fetch_company_data(symbol):
    """
    Task to download or update company metadata. Queue via send method.
    """
    company = Company.objects.get(symbol=symbol)
    response = requests.get(
        f"https://autocomplete.clearbit.com/v1/companies/suggest?query={symbol}"
    )
    logging.info(
        f"Downloaded data for company {symbol} with response {response.status_code}"
    )

    data = response.json()

    if isinstance(data, list) and data:
        best_candidate = data[0]
        logging.info("Fetching data")
        company.name = best_candidate["name"]
        company.website = best_candidate["domain"]
        company.logo = best_candidate["logo"]
        company.save()
    else:
        raise ValueError(f"Unsupported datatype: ${repr(data)}")

    update_company_stats.send(symbol)


@dramatiq.actor
def update_company_stats(symbol):
    """
    Task to download company statistics for period given. Queue via send method.
    """
    company = Company.objects.get(symbol=symbol)
    response = requests.get(
        f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={symbol}&apikey={settings.ALPHAVANTAGE_API_KEY}"
    )

    logging.info(
        f"Downloaded stock data for company ${company.name} ({company.symbol}) with response {response.status_code}"
    )

    data = response.json()

    for raw_date in data["Time Series (Daily)"]:
        date = datetime.datetime.strptime(raw_date, "%Y-%m-%d").date()

        raw_data = data["Time Series (Daily)"][raw_date]
        daily_data = {
            "date": date,
            "volume": int(raw_data["5. volume"]),
            "open": float(raw_data["1. open"]),
            "close": float(raw_data["4. close"]),
            "high": float(raw_data["2. high"]),
            "low": float(raw_data["3. low"]),
        }
        instance, created = company.statistics_set.get_or_create(
            date=date, defaults=daily_data
        )
        if not created:
            for attr, value in daily_data.items():
                setattr(instance, attr, value)
            instance.save()


def update_stats_all_companies():
    """
    Fetch and update stock stats for each company.
    """
    logging.info(f"Sending refresh order for all companies")
    for company in Company.objects.all():
        update_company_stats.send(company.symbol)
