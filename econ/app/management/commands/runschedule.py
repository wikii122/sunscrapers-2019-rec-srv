import schedule
import time

from django.core.management.base import BaseCommand

from econ.app.tasks import update_stats_all_companies

class Command(BaseCommand):
    help = 'Runs periodic tasks'

    def handle(self, *args, **options):
        schedule.every().hour.do(update_stats_all_companies)

        while True:
            schedule.run_pending()
            time.sleep(1)