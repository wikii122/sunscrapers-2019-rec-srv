from django.db import models

# Create your models here.
class Company(models.Model):
    """
    Company data summary.
    """

    name = models.CharField(max_length=64, blank=True, null=True)
    symbol = models.CharField(max_length=8, unique=True)
    website = models.CharField(max_length=64, blank=True, null=True)
    logo = models.CharField(max_length=64, blank=True, null=True)


class Statistics(models.Model):
    """
    Company statistics per day.
    """

    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    open = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    close = models.FloatField()
    volume = models.IntegerField()
    date = models.DateField()

